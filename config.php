<?php

/**
 * 411 configuration file
 *
 * Rename this to 'config.php' and modify.
 */
$config = [];

/**
 * Authentication configuration
 */
$config['auth'] = [
    'proxy' => [
        /**
         * Whether to enable proxy auth.
         * IT IS VERY INSECURE TO ENABLE THIS IF 411 is not run behind an auth proxy.
         */
        'enabled' => false,
        /**
         * The name of the header the proxy is setting.
         */
        'header' => null,
        /**
         * Whether to automatically create users who are authenticated.
         */
        'auto_create' => false,
        /**
         * Email domain for automatically created users.
         */
        'domain' => null,
    ],
    'api' => [
        /**
         * Whether to enable api access to 411.
         */
        'enabled' => true
    ]
];

/**
 * Database configuration
 */
$config['db'] = [
    /**
     * A PDO compatible DSN. See https://secure.php.net/manual/en/pdo.drivers.php for details.
     * SQLite is the default configuration but MySQL is also supported. To configure the latter,
     * you'll need a dsn like the following: 'mysql:host=localhost;dbname=fouroneone'.
     */
    'dsn' => 'mysql:host=MYSQL_HOST;dbname=MYSQL_DATABASE',
    /**
     * The user name for connecting to the database.
     */
    'user' => 'MYSQL_USER',
    /**
     * The password for connecting to the database. Optional if the PDO driver doesn't
     * require a password.
     */
    'pass' => 'MYSQL_PASS',
];


/****
 *
 * Search type configuration
 * Note: All hostnames should specify protocol!
 *
 ***/

/**
 * Elasticsearch search type
 */
$config['elasticsearch'] = [
    /**
     * Each entry in this array represents an Elasticsearch source that 411 can query.
     *
     * 'hosts': An array of hosts powering your ES cluster.
     * 'index_hosts': An array of hosts to use for indexing (if different from 'hosts').
     * 'ssl_cert': Path to an ssl certificate if your cluster uses HTTPS.
     * 'index': The index to query. Leave as null to query all indices.
     *          If the index is date_based, accepts index patterns. (Otherwise, it's taken literally)
     *          Any characters wrapped by [] will be taken literally.
     *          All other characters are interpretted via PHP's date formatting syntax.
     *          See https://secure.php.net/manual/en/function.date.php for details.
     * 'date_based': Whether to generate an index name according to a date-based index pattern.
     * 'date_interval': If the index is date_based, this defines the indexing pattern interval.
     *                  'h' - Hourly.
     *                  'd' - Daily.
     *                  'w' - Weekly.
     *                  'm' - Monthly.
     *                  'y' - Yearly.
     * 'date_field': The field to use for doing date-based queries.
     * 'date_type': The format of the date field.
     *              null - Automatically detect and parse. Should work most of the time!
     *              '@' - Parse as a UNIX timestamp.
     *              '#' - Parse as a UNIX timestamps (in milliseconds).
     *              All other strings are interpretted via PHP's date formatting syntax.
     * 'src_url': A format string for generating default source links.
     *            Requires the following format specifiers: 's', 'd', 'd'.
     *            Ex: 'https://localhost/?query=%s&from=%d&to=%d'
     */
    'alerts' => [
        'hosts' => ['https://elastic:changeme@es-master:9200'],
        'index_hosts' => [],
        'ssl_cert' => '/elasticsearch/config/searchguard/ssl/elastic.crtfull.pem',
        'index' => null,
        'date_based' => false,
        'date_field' => 'alert_date',
        'src_url' => null,
    ],
    # Configuration for the logstash index that 411 queries.
    'logstash' => [
        'hosts' => ['https://elastic:changeme@es-master:9200'],
        'index_hosts' => [],
        'ssl_cert' => '/elasticsearch/config/searchguard/ssl/elastic.crtfull.pem',
        'index' => '[logstash-]Y.m.d',
        'date_based' => true,
        'date_field' => '@timestamp',
        'src_url' => null,
    ],
    # Configuration for the ossec index that 411 queries.
    'wazuh' => [
        'hosts' => ['https://elastic:changeme@es-master:9200'],
        'index_hosts' => [],
        'ssl_cert' => '/elasticsearch/config/searchguard/ssl/elastic.crtfull.pem',
        'index' => '[wazuh-alerts-3.x-]Y.m.d',
        'date_based' => true,
        'date_field' => '@timestamp',
        'src_url' => null,
    ],
   # Configuration for the syslog index that 411 queries.
    'syslog' => [
        'hosts' => ['https://elastic:changeme@es-master:9200'],
        'index_hosts' => [],
        'ssl_cert' => '/elasticsearch/config/searchguard/ssl/elastic.crtfull.pem',
        'index' => '[syslog-]Y.m.d',
        'date_based' => true,
        'date_field' => '@timestamp',
        'src_url' => null,
    ],
   # Configuration for the apache index that 411 queries.
    'apache' => [
        'hosts' => ['https://elastic:changeme@es-master:9200'],
        'index_hosts' => [],
        'ssl_cert' => '/elasticsearch/config/searchguard/ssl/elastic.crtfull.pem',
        'index' => '[logstash-apache-]Y.m.d',
        'date_based' => true,
        'date_field' => '@timestamp',
        'src_url' => null,
    ],
   # Configuration for the suricata index that 411 queries.
    'suricata' => [
        'hosts' => ['https://elastic:changeme@es-master:9200'],
        'index_hosts' => [],
        'ssl_cert' => '/elasticsearch/config/searchguard/ssl/elastic.crtfull.pem',
        'index' => '[suricata-]Y.m.d',
        'date_based' => true,
        'date_field' => '@timestamp',
        'src_url' => null,
    ],
   # Configuration for the logstash-win index that 411 queries.
    'windows' => [
        'hosts' => ['https://elastic:changeme@es-master:9200'],
        'index_hosts' => [],
        'ssl_cert' => '/elasticsearch/config/searchguard/ssl/elastic.crtfull.pem',
        'index' => '[eventlog-]Y.m.d',
        'date_based' => true,
        'date_field' => '@timestamp',
        'src_url' => null,
    ],
   # Configuration for the PaloAlto indices that 411 queries.
    'panos-traffic' => [
        'hosts' => ['https://elastic:changeme@es-master:9200'],
        'index_hosts' => [],
        'ssl_cert' => '/elasticsearch/config/searchguard/ssl/elastic.crtfull.pem',
        'index' => '[panoa-traffic-]Y.m.d',
        'date_based' => true,
        'date_field' => '@timestamp',
        'src_url' => null,
    ],
    'panos-threat' => [
        'hosts' => ['https://elastic:changeme@es-master:9200'],
        'index_hosts' => [],
        'ssl_cert' => '/elasticsearch/config/searchguard/ssl/elastic.crtfull.pem',
        'index' => '[panos-threat-]Y.m.d',
        'date_based' => true,
        'date_field' => '@timestamp',
        'src_url' => null,
    ],
    'panos-config' => [
        'hosts' => ['https://elastic:changeme@es-master:9200'],
        'index_hosts' => [],
        'ssl_cert' => '/elasticsearch/config/searchguard/ssl/elastic.crtfull.pem',
        'index' => '[panos-config-]Y.m.d',
        'date_based' => true,
        'date_field' => '@timestamp',
        'src_url' => null,
    ],
    'panos-system' => [
        'hosts' => ['https://elastic:changeme@es-master:9200'],
        'index_hosts' => [],
        'ssl_cert' => '/elasticsearch/config/searchguard/ssl/elastic.crtfull.pem',
        'index' => '[panos-system-]Y.m.d',
        'date_based' => true,
        'date_field' => '@timestamp',
        'src_url' => null,
    ],
    # Configuration for the bro index that 411 queries.
    'bro' => [
        'hosts' => ['https://elastic:changeme@es-master:9200'],
        'index_hosts' => [],
        'ssl_cert' => '/elasticsearch/config/searchguard/ssl/elastic.crtfull.pem',
        'index' => '[bro-]Y.m.d',
        'date_based' => true,
        'date_field' => '@timestamp',
        'src_url' => null,
    ],
   # Configuration for Microsoft-Windows-Sysmon/Operational
    'windows-sysmon' => [
        'hosts' => ['https://elastic:changeme@es-master:9200'],
        'index_hosts' => [],
        'ssl_cert' => '/elasticsearch/config/searchguard/ssl/elastic.crtfull.pem',
        'index' => '[logs-endpoint-winevent-sysmon-]Y.m.d',
        'date_based' => true,
        'date_field' => '@timestamp',
        'src_url' => null,
    ],
   # Configuration for Windows Security Logs
    'windows-security' => [
        'hosts' => ['https://elastic:changeme@es-master:9200'],
        'index_hosts' => [],
        'ssl_cert' => '/elasticsearch/config/searchguard/ssl/elastic.crtfull.pem',
        'index' => '[logs-endpoint-winevent-security-]Y.m.d',
        'date_based' => true,
        'date_field' => '@timestamp',
        'src_url' => null,
    ],
   # Configuration for Windows System Logs
    'windows-system' => [
        'hosts' => ['https://elastic:changeme@es-master:9200'],
        'index_hosts' => [],
        'ssl_cert' => '/elasticsearch/config/searchguard/ssl/elastic.crtfull.pem',
        'index' => '[logs-endpoint-winevent-system-]Y.m.d',
        'date_based' => true,
        'date_field' => '@timestamp',
        'src_url' => null,
    ],
   # Configuration for Windows Application Logs
    'windows-application' => [
        'hosts' => ['https://elastic:changeme@es-master:9200'],
        'index_hosts' => [],
        'ssl_cert' => '/elasticsearch/config/searchguard/ssl/elastic.crtfull.pem',
        'index' => '[logs-endpoint-winevent-application-]Y.m.d',
        'date_based' => true,
        'date_field' => '@timestamp',
        'src_url' => null,
    ],
    # Configuration for Windows Powershell Logs
    'windows-powershell' => [
        'hosts' => ['https://elastic:changeme@es-master:9200'],
        'index_hosts' => [],
        'ssl_cert' => '/elasticsearch/config/searchguard/ssl/elastic.crtfull.pem',
        'index' => '[logs-endpoint-winevent-powershell-]Y.m.d',
        'date_based' => true,
        'date_field' => '@timestamp',
        'src_url' => null,
    ],
   # Configuration for Windows WMI Activity
    'windows-wmi' => [
        'hosts' => ['https://elastic:changeme@es-master:9200'],
        'index_hosts' => [],
        'ssl_cert' => '/elasticsearch/config/searchguard/ssl/elastic.crtfull.pem',
        'index' => '[logs-endpoint-winevent-wmiactivity-]Y.m.d',
        'date_based' => true,
        'date_field' => '@timestamp',
        'src_url' => null,
    ],
];

/**
 * Graphite
 *
 * Configure to allow querying Graphite.
 */
$config['graphite'] = [
    /**
     * Each entry in this array represents a Graphite source that 411 can query.
     *
     * 'host': The hostname for your Graphite instance.
     */
    'graphite' => [
        'host' => null,
    ],
];

/**
 * ThreatExchange
 * See https://developers.facebook.com/products/threat-exchange for details.
 *
 * Configure to allow querying ThreatExchange.
 */
$config['threatexchange'] = [
    /**
     * The api token for connecting to ThreatExchange.
     */
    'api_token' => null,
    /**
     * The api secret for connecting to ThreatExchange.
     */
    'api_secret' => null,
];


/****
 *
 * Target configuration
 *
 ***/

/**
 * Jira
 *
 * Fill in to enable Jira integration.
 */
$config['jira'] = [
    /**
     * The hostname for your Jira instance.
     */
    'host' => null,
    /**
     * The username for connecting to Jira.
     */
    'user' => null,
    /**
     * The password for connecting to Jira.
     */
    'pass' => null,
];

/**
 * Slack
 *
 * Fill in to enable Slack integration.
 */
$config['slack'] = [
    /**
     * A webhook url to push Alerts to.
     * See https://api.slack.com/incoming-webhooks for details.
     */
    'webhook_url' => null
];

