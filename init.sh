#!/bin/bash
if [ -z "$(ls -A /data/config.php)" ]; then
    cp -a /.backup/data/config.php /data
fi

source /etc/apache2/envvars
export FOURONEONEHOST=$HOSTNAME
# /usr/bin/sqlite3 /data/data.db "update sites set host='$(hostname)' where site_id=1;"
while ! mysqladmin ping -h$MYSQL_HOST --silent; do
    sleep 1
done
mysql -h$MYSQL_HOST -u $MYSQL_USER -p$MYSQL_PASS $MYSQL_DATABASE -e "update sites set host='$(hostname)' where site_id=$SITE_ID"
sed -i 's@MYSQL_HOST@'"$MYSQL_HOST"'@' /data/config.php
sed -i 's@MYSQL_PASS@'"$MYSQL_PASS"'@' /data/config.php
sed -i 's@MYSQL_DATABASE@'"$MYSQL_DATABASE"'@' /data/config.php
sed -i 's@MYSQL_USER@'"$MYSQL_USER"'@' /data/config.php
sed -i 's@changeme@'"$ELASTIC_PWD"'@' /data/config.php

if [ -f "/.backup/elasticsearch/config/searchguard/ssl/elastic.crtfull.pem" ]; then
    mkdir -p /elasticsearch/config/searchguard/ssl/
    cp /.backup/elasticsearch/config/searchguard/ssl/elastic.crtfull.pem /elasticsearch/config/searchguard/ssl/
    chown www-data:www-data /elasticsearch/config/searchguard/ssl/elastic.crtfull.pem 
    chmod 440 /elasticsearch/config/searchguard/ssl/elastic.crtfull.pem
fi